package com.tower.crtn.test.mockito.junit.service;

import com.tower.crtn.test.mockito.junit.model.CustomerModel;
import com.tower.crtn.test.mockito.junit.model.CustomerUpdateModel;

import java.util.List;

public interface CustomerService {

  boolean saveCustomer(CustomerModel customer);

  boolean updateCustomer(int customerId, CustomerUpdateModel customer);

  boolean deleteCustomer(int customerId);

  List<CustomerModel> findAllCustomers();

  CustomerModel findByCustomerId(int customerId);

  CustomerModel findByFullName(String name, String lastName);

  List<CustomerModel> findAllByStatusCode(String statusCode);
}
